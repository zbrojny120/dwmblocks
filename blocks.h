//Modify this file to change what commands output to your statusbar, and recompile using the make command.

static char time[] =
	"date +%H:%M";
static char backlight_status[] =
	"cat /sys/class/backlight/intel_backlight/brightness";
static char free_memory[] = 
	"free -h | head -n2 | tail -n1 | "
	"sed -E 's/Mem: *([0-9]+\\.?[0-9]+..) *([0-9]+\\.?[0-9]+..).*/\\2\\/\\1/g'";
static char battery_capacity[] =
	"echo $(upower --show-info /org/freedesktop/UPower/devices/DisplayDevice | rg percentage | awk '{print $2}' | cut -d. -f1)%";
static char sound_volume[] =
	"echo $(pamixer --get-volume)%";
static const Block blocks[] = {
	/*Icon*/   /*Command*/	    /*Update Interval*/    /*Update Signal*/
	{"Light: ",	backlight_status,		0,		1},
	{"Mem: ", 	free_memory,			10,		0},
	{"Batt: ", 	battery_capacity,		60,		0},
	{"Vol: ",	sound_volume,			0,		2},
	{"",		time,					1,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char* delim = " | ";
